var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;

//how user isstored in session
passport.serializeUser(function(user, done){
    done(null, user.id);//whenever user stored in session, serialize the user by id
});

passport.deserializeUser(function(id, done){
    User.findById(id, function(err, user){
        done(err, user);
    });
});

passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done){
    User.findOne({'emial':email}, function(err, user){
        if(err) {
            return done(err);
        }
        if(user){
            return done(null, false, {message : 'Email is already in use'});
        }
        var newUser = new User();
        newUser.emial = email;
        newUser.password = newUser.encryptPassword(password);
        newUser.save(function(err, result){
            if(err) {
                return done(err);
            }
            return done(null, newUser);
        });
    })
}));