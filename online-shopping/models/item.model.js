var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var itemSchema = new Schema({
    uniqueId : {type: String, required: true}, 
    title : {type: String, required: true}, 
    doctype : {type: String, required: true}, 
    shortDesc : {type: String, required: true}, 
    longDesc : {type: String, required: true}, 
    itemCategory : {
        mainCategories : [
            {type: String}
        ], 
        subCategories : [
            {type: String}
        ]
    }, 
    itemQty : {
        qtyAsWeight : [
            {type: String}
        ], 
        qtyAsUnits : {type: Number, default: 0}
    }, 
    itemImageUrls : [
        {type: String}
    ], 
    itemPrice : {
        retailPrice : {type: Number, required: true}, 
        salePrice : {
            price : {type: Number, default: 0}, 
            saleEndDate : {type: Date, default: Date.now}
        }
    }, 
    discountPercentage : {type: String, required: false}, 
    totalInventory : {type: Number, required: false}, 
    seasonTag : {type: String, default: "Null"}, 
    isActive : {type: Boolean, required: true}, 
    reviewScore : {type: Number, required: true}, 
    reviewText : {type: String, required: true}, 
    benefits : {
        nutritionalFacts : {type: String, required: true}, 
        benefitsFacts : {type: String, required: true}
    }
});

//Item : schema name   
module.exports = mongoose.model('Item', itemSchema);

