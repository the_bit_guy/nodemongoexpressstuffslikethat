
var express = require('express');
var router = express.Router();
var Item = require('../models/item.model');
var csrf = require('csurf');
var passport = require('passport');

var csrfProtection = csrf();

router.use(csrfProtection); //all the routes included in this package should be pritected by csrf


/* GET home page. */
router.get('/', function(req, res, next) {
  Item.find(function(err, docs){
    //res.set('partials/popular-items', {items : docs});
    res.render('index', { title: 'Express', items : docs});
  });
 });


router.get('/user/register', function(req, res, next){
  console.log("11111111111111111111");
  res.render('user/register', {csrfToken : req.csrfToken()});
}); 

router.post('/user/register', passport.authenticate('local.signup', {
  successRedirect: '/user/profile',
  failureRedirect: '/user/register',
  failureFlash:  true
}));

router.get('/user/profile', function(req, res, next){
  res.render('user/account-profile');
});

module.exports = router;

