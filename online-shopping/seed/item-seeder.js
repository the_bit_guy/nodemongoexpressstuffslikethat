var Item = require('../models/item.model');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/swastibazar');

var items = [new Item({ 
    uniqueId : "PT0011", 
    title : "Potato", 
    doctype : "ITEMS", 
    shortDesc : "Potato", 
    longDesc : "Fresho Potatoes are nutrient-dense, non-fattening and have reasonable amount of calories. Include them in your regular meals so that the body receives a good supply of carbohydrates, dietary fibers and essential minerals such as copper, magnesium, and iron. In India, potatoes are probably the second-most consumed vegetables after onions.", 
    itemCategory : {
        mainCategories : [
            "vegitable"
        ], 
        subCategories : [
            "tomato"
        ]
    }, 
    itemQty : {
        qtyAsWeight : [
            "1kg", 
            "2kg"
        ], 
        qtyAsUnits : 0
    }, 
    itemImageUrls : [
        "images/product/Cucumbers.jpg"
    ], 
    itemPrice : {
        retailPrice : 15, 
        salePrice : {
            price : 0, 
            saleEndDate : null
        }
    }, 
    discountPercentage : 0, 
    totalInventory : 0, 
    seasonTag : "Null", 
    isActive : true, 
    reviewScore : 3, 
    reviewText : "I got a fresh vegitables delivery", 
    benefits : {
        nutritionalFacts : "The vegetable is loaded with Antioxidants and Vitamin C which is great for strengthening immunity and fighting diseases.", 
        benefitsFacts : "tomatoes should be included in the diet of those having mouth ulcers. As they are easy to digest, they are good for patients. Consumption of tomatoes helps to maintain the blood glucose level and keeps the brain alert and active. People who are diagnosed with kidney stones or heart disorders can include tomatoes in their diet as they are light on the stomach."
    }
}),
new Item({ 
    uniqueId : "PT0012", 
    title : "Cucumber", 
    doctype : "ITEMS", 
    shortDesc : "Cucumber", 
    longDesc : "Fresho Cucumber are nutrient-dense, non-fattening and have reasonable amount of calories. Include them in your regular meals so that the body receives a good supply of carbohydrates, dietary fibers and essential minerals such as copper, magnesium, and iron. In India, potatoes are probably the second-most consumed vegetables after onions.", 
    itemCategory : {
        mainCategories : [
            "vegitable"
        ], 
        subCategories : [
            "cucumber", "green"
        ]
    }, 
    itemQty : {
        qtyAsWeight : [
            "1kg", 
            "2kg"
        ], 
        qtyAsUnits : 0
    }, 
    itemImageUrls : [
        "images/product/Cucumbers.jpg"
    ], 
    itemPrice : {
        retailPrice : 20, 
        salePrice : {
            price : 0, 
            saleEndDate : null
        }
    }, 
    discountPercentage : 0, 
    totalInventory : 0, 
    seasonTag : "Null", 
    isActive : true, 
    reviewScore : 4, 
    reviewText : "I got a fresh vegitables delivery", 
    benefits : {
        nutritionalFacts : "The vegetable is loaded with Antioxidants and Vitamin C which is great for strengthening immunity and fighting diseases.", 
        benefitsFacts : "tomatoes should be included in the diet of those having mouth ulcers. As they are easy to digest, they are good for patients. Consumption of tomatoes helps to maintain the blood glucose level and keeps the brain alert and active. People who are diagnosed with kidney stones or heart disorders can include tomatoes in their diet as they are light on the stomach."
    }
})
];

var done = 0;
for(var i = 0; i < items.length; i++) {
    items[i].save(function(){
        done++;
        if(done === items.length){
            mongoose.disconnect();
        }
    });
}



