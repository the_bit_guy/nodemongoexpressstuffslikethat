#install express
npm install -g express-generator

#create express project
express project-folder-name

#start express
npm start (from inside the preoject folder)
OR
nodemon start (before start run 'npm install' in the project folder)

#installing express-handlebars
npm install express-handlebars --save

#installing express-validator
npm install -g express-validator

#installing the express-session
npm install express-session --save