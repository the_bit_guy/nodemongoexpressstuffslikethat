var url = require('url');
var fs = require('fs');

function readHTML(path, response) {
    fs.readFile(path, null, function(error, data){
        if (error) {
            response.writeHead(404);
            response.writeHead('File not found');
        } else {
            response.write(data);
        }
        response.end();
    });
}

module.exports = {
    handleRequest : function(request, response) {
        response.writeHead(200, {'contentType':'text/html'});

        var path = url.parse(request.url).pathname;
        switch (path) {
            case '/':
                readHTML('./index.html', response);
                break;
            case '/login':
                readHTML('./login.html', response);
                break;
            default:
                response.writeHead(404);
                response.write('Route Not defined');
                response.end();
        }
    }
}