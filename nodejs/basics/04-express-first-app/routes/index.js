var express = require('express');
var router = express.Router();
var mongoose = require('mongoose'); 
mongoose.connect('mongodb://localhost:27017/swastibazar');

var Schema = mongoose.Schema;

var userDataSchema = new Schema({
  title :  {type : String, required : true},
  content : String,
  author : String
}); 


var UserData = mongoose.model('UserData', userDataSchema);
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Welcome to Express', condition: true, anyArray: [1, 2, 3] }); //this 2nd parameter will b epasssed along with the 2nd prameter
});

router.get('/get-data', function(req, res, next){
  UserData.find().then(function(doc){
    res.render('index', {items : doc});
  });
});

router.get('/test/:id', function(req, res, next){
  res.render('test', {firstParam : req.params.id});
});

router.post('/insert', function(req, res, next){
  var items = {
    title : req.body.title,
    author : req.body.author,
    content : req.body.content
  };

  var data = new UserData(items);
  data.save();

  res.redirect('/');
});



module.exports = router;
