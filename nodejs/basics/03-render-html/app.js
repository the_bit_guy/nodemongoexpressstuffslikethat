var fs = require('fs');

function readHTML(path, response) {
    fs.readFile(path, null, function(error, data){
        if (error) {
            response.writeHead(404);
            response.writeHead('File not found');
        } else {
            response.write(data);
        }
        response.end();
    });
}

module.exports = {
    handleRequest : function(request, response) {
        response.writeHead(200, {'content-Type' : 'text/html'});
        readHTML('./index.html', response);
        //response.end();
    }
}
